<!DOCTYPE html>
<html>
<head>
    @include('admin.layouts.head')
</head>
<body class="hold-transition">
<!-- Site wrapper -->
<div class="wrapper">
    <!-- Navbar -->
    @include('admin.layouts.navbar')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('admin.layouts.mainSideBar')

   @section('admin-main-content')
       @show

@include('admin.layouts.footer')
</body>
</html>
