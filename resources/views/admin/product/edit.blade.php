@extends('admin.layouts.app')

@section('admin-head')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
@endsection

@section('admin-main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Post Editor</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Post Editor</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- inserted from general form elements file -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Titles</h3>
                        </div>
                        <!-- /.card-header -->
                    @include('includes.messages')
                    <!-- form start -->
                        <form role="form" action="{{ route('products.update', $product->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-5 mr-4">
                                        <div class="form-group">
                                            <label for="title">Product Title</label>
                                            <input type="text" class="form-control" id="title" name="title" value="{{ $product->title }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="subtitle">Product Price</label>
                                            <input type="text" class="form-control" id="price" name="price" value="{{ $product->price }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="slug">Subtitle</label>
                                            <input type="text" class="form-control" id="subtitle" name="subtitle" value="{{ $product->subtitle }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-5 ml-3">
                                        <div class="form-group" data-select2-id="37">
                                            <label>Select Categories</label>
                                            <select class="form-control" name="category_id">
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}"
                                                            @if($product->category->id == $category->id)
                                                                selected
                                                            @endif
                                                    >{{ $category->categoryTitle }}</option>
                                                @endforeach
                                            </select>
                                            {{--                                                <span class="select2 select2-container select2-container--default select2-container--below" dir="ltr" data-select2-id="8" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1" aria-disabled="false"><ul class="select2-selection__rendered"><li class="select2-selection__choice" title="Alabama" data-select2-id="49"><span class="select2-selection__choice__remove" role="presentation">×</span>Alabama</li><li class="select2-selection__choice" title="Delaware" data-select2-id="50"><span class="select2-selection__choice__remove" role="presentation">×</span>Delaware</li><li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" placeholder="" style="width: 0.75em;"></li></ul></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>--}}
                                        </div>
                                        <div class="form-group">
                                            <label for="slug">Availability</label>
                                            <select class="form-control" style="width: 100%;" name="stock">
                                                <option value="1" @if($product->stock == 1) selected @endif>Available</option>
                                                <option value="0" @if($product->stock == 0) selected @endif>Not Available</option>
                                            </select>
                                        </div>
                                        <div class="row justify-content-between">
                                            <div class="form-group col-lg-10">
                                                <label for="file">File input</label>
                                                <div class="input-group">
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="image1" name="image1">
                                                        <label class="custom-file-label" for="image">Choose file</label>
                                                        <input type="hidden" name="image">
                                                    </div>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text" id="imageUpload">Upload</span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card card-outline card-info">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        Post body
                                        <small>Simple and fast</small>
                                    </h3>
                                    <!-- tools box -->
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                                                title="Collapse">
                                            <i class="fas fa-minus"></i></button>
                                    </div>
                                    <!-- /. tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body pad">
                                    <div class="mb-3">
                                        <textarea class="textarea" placeholder="Place some text here" name="description" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $product->description }}</textarea>
                                    </div>
                                    <p class="text-sm mb-0">
                                        Editor <a href="https://github.com/summernote/summernote">Documentation and license
                                            information.</a>
                                    </p>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                                <a href="{{ route('products.index') }}" class="btn btn-warning">Back</a>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->

                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('admin-footer')
    <!-- Summernote -->
    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.select2').select2();
            bsCustomFileInput.init();
        });
        $(function () {
            // Summernote
            $('.textarea').summernote()
        })
    </script>
@endsection
