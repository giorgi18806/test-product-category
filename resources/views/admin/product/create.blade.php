@extends('admin.layouts.app')

@section('admin-head')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/bootstrap-wysihtml5//bootstrap-wysihtml5.min.css') }}">
    <link rel="stylesheet" href="{{ asset('user/css/prism.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
@endsection

@section('admin-main-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Create New Product</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
                            <li class="breadcrumb-item active">Create New Product</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- inserted from general form elements file -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Titles</h3>
                        </div>
                        <!-- /.card-header -->
                        @include('includes.messages')
                        <!-- form start -->
                        <form role="form" action="{{ route('products.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-5 mr-4">
                                        <div class="form-group">
                                            <label for="title">Product Title</label>
                                            <input type="text" class="form-control" id="title" name="title" placeholder="Enter title">
                                        </div>
                                        <div class="form-group">
                                            <label for="price">Product Price</label>
                                            <input type="text" class="form-control" id="price" name="price" placeholder="Enter price">
                                        </div>

                                        <div class="form-group">
                                            <label for="subtitle">Subtitle</label>
                                            <input type="text" class="form-control" id="subtitle" name="subtitle" placeholder="Enter subtitle">
                                        </div>
                                    </div>
                                    <div class="col-lg-5 ml-3">
                                        <div class="form-group">
                                            <label>Category</label>
                                            <select class="form-control" name="category_id">
                                                @foreach($categories as $category)--}}
                                                    <option value="{{ $category->id }}">{{ $category->categoryTitle }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="stock">Availability</label>
                                            <select class="form-control" name="stock">
                                                <option selected="selected" value="1">Available</option>
                                                <option value="0">Not Available</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="image1">File input</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="image" name="image1" style="cursor: pointer">
                                                    <label class="custom-file-label" for="image">Choose file</label>
                                                    <input type="hidden" name="image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card card-outline card-info">

                                <div class="card-body pad">
                                    <div class="mb-3">
                                        <textarea class="textarea" placeholder="Place some text here" name="description" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    </div>
{{--                                    <p class="text-sm mb-0">--}}
{{--                                        Editor <a href="https://github.com/summernote/summernote">Documentation and license--}}
{{--                                            information.</a>--}}
{{--                                    </p>--}}
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{ route('products.index') }}" class="btn btn-warning">Back</a>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->

                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('admin-footer')
    <!-- Summernote -->
{{--    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>--}}
    <script src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('user/js/prism.js') }}"></script>
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.select2').select2();
            bsCustomFileInput.init();
        });
        // $(function () {
        //     // Summernote
        //     $('.textarea').summernote()
        // })
        $(function () {
            CKEDITOR.replace('description');
            $('.textarea').wysihtml5();
        });
    </script>
@endsection
