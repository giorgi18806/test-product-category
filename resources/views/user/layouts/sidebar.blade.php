<div class="col-lg-3">

    <h1 class="my-4">Shop ACME</h1>
    <div class="list-group">
        @foreach($categories as $category)
            <a href="{{ route('category', $category->id) }}" class="list-group-item">{{ $category->categoryTitle }}</a>
        @endforeach
    </div>

</div>
