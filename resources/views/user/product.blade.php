@extends('user.app')

@section('main-content')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v9.0" nonce="O4l8Rd7P"></script>
    <article>
        <div class="container">
            <div class="row">
                <div class="card mt-4">
                    <img class="card-img-top img-fluid" src="{{ Storage::disk('local')->url($product->image) }}" alt="{{ $product->title }}">
                    <div class="card-body">
                        <a href="#" class="btn btn-success float-right mt-1">Add to Cart</a>
                        <h3 class="card-title">{{ $product->title }}</h3>
                        <h4>${{ $product->price }}</h4>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente dicta fugit fugiat hic aliquam itaque facere, soluta. Totam id dolores, sint aperiam sequi pariatur praesentium animi perspiciatis molestias iure, ducimus!</p>
                        <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
                        4.0 stars
                    </div>
                </div>
                <div class="fb-comments" data-href="{{ Request::url() }}" data-width="" data-numposts="10"></div>
            </div>
        </div>
    </article>

    <hr>
@endsection
