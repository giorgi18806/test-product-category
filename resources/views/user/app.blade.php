<!DOCTYPE html>
<html lang="en">

@include('user.layouts.head')

<body>

<!-- Navigation -->
@include('user.layouts.navigation')

<!-- Page Content -->
<div class="container">

    <div class="row">

        @include('user.layouts.sidebar')
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">

            @section('main-content')
            @show

        </div>
        <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<!-- Footer -->
@include('user.layouts.footer')

</body>

</html>
