@extends('user.app')

@section('main-content')
    @include('user.layouts.carousel')

    <div class="row">
        @foreach($products as $product)
            <div class="col-lg-4 col-md-6 mb-4">
                <div class="card h-100">
                    <a href="{{ route('product', $product->id) }}"><img class="card-img-top" src="{{ Storage::disk('local')->url($product->image) }}" alt=""></a>
                    <div class="card-body">
                        <h4 class="card-title">
                            <a href="{{ route('product', $product->id) }}">{{ $product->title }}</a>
                        </h4>
                        <h5>${{ $product->price }}</h5>
                        <p class="card-text">{!! htmlspecialchars_decode($product->subtitle) !!}</p>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                    </div>
                </div>
            </div>
        @endforeach


    </div>
    <!-- /.row -->

    <hr>
@endsection
