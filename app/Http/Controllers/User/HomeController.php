<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;


class HomeController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $products = Product::all();
        $categories = Category::all();
        return view('user.index', compact('products', 'categories'));
    }

    /**
     * @param Category $category
     * @return Application|Factory|View
     */
    public function category (Category $category)
    {
        $products = $category->products;
        $categories = Category::all();

        return view('user.index', compact('products', 'categories'));
    }
}
