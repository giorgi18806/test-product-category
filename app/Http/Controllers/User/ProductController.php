<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\User\Post;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('user.index');
    }

    /**
     * @param Product $product
     * @return Application|Factory|View
     */
    public function product(Product $product)
    {
        $categories = Category::all();
        return view('user.product', compact('product', 'categories'));
    }


}
