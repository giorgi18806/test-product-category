<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lorem = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis et enim aperiam inventore, similique necessitatibus neque non! Doloribus, modi sapiente laboriosam aperiam fugiat laborum. Sequi mollitia, necessitatibus quae sint natus.';
        $products = [
            ['Samsung Galaxy S20', 1, 'Find the best price for the Galaxy S20', 1190, 1, 'public/images/3DKDefUGN0LlySFneXfrc9IJQxTdJ3HdFBEVA7A7.jpg'],
            ['iPhone 12', 1, 'Blast past fast. 5G speed. A14 Bionic, the fastest chip in a smartphone', 1450, 0, 'public/images/KOmwizm1dpeRxSQONKTJnLbNtKeyODAaG9euShIl.jpg'],
            ['Lenovo Tab4 10',2, 'Beautifully designed, powerfully built', 250, 1, 'public/images/8lwmuGsmlwmXe5xM4kAKsF07gaqCwTc9uglkSwQp.jpg'],
            ['HP EliteBook',3, 'Shop Best Buy for HP laptops', 849, 0, 'public/images/3k0ZuJYETMV5uxCEO1j3pbrNxOr9cYaZ7QC9M69j.png'],
            ['Macbook Air',3, 'MacBook Air comes with the new scissor-switch keyboard', 1319, 1, 'public/images/0mdfxRtBeEZp6g1s2kpSyesR9OuKGUgj0G1exhvQ.jpg'],
            ['Gearbrain',4, 'Best Bluetooth wireless earphones for iPhone', 88, 1, 'public/images/SffFyyf3AzkUpRBi78bOQPUiwjRuWuXgTj5ytO3p.png'],
            ['Powerbeats',4, 'Best Buy Beats - Powerbeats Pro Totally Wireless Earphones - Black', 75, 1, 'public/images/ehQXMPWcpBXFwT1iUxH5rGZazDatcXOYfe88TaLp.jpg'],
        ];

        for ($i = 0; $i < count($products); $i++) {
            DB::table('products')->insert([
                'title' => $products[$i][0],
                'category_id' => $products[$i][1],
                'subtitle' => $products[$i][2],
                'description' => $lorem,
                'price' => $products[$i][3],
                'stock' => $products[$i][4],
                'image' => $products[$i][5],
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }
}
