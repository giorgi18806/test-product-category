<?php

use App\Http\Controllers\Admin\HomeController;
use Illuminate\Support\Facades\Route;

Route::get('/',  'App\Http\Controllers\User\HomeController@index')->name('home');
Route::get('product/{product}', 'App\Http\Controllers\User\ProductController@product')->name('product');
Route::get('post/category/{category}', [\App\Http\Controllers\User\HomeController::class, 'category'])->name('category');


Route::get('admin/home', [HomeController::class, 'index'])->name('admin.home');

Route::resource('admin/products', App\Http\Controllers\Admin\ProductController::class);
Route::resource('admin/category', App\Http\Controllers\Admin\CategoryController::class);

